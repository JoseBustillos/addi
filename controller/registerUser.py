import csv
from tkinter import messagebox


def registerUser(all_name, user, password, birth_date):
    if (
        not len(all_name.get()) > 0
        and len(user.get()) > 0
        and len(password.get()) > 0
        and len(birth_date.get() > 0)
    ):
        messagebox.showerror("Error", "Ingrese Usuario y contrasena")
    else:
        header = ["all_name", "user", "password", "birth_date"]
        data = [all_name, user, password, birth_date]

        with open("credentials.csv", "w", encoding="UTF8", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(header)
            writer.writerow(data)
        messagebox.showerror("Exito", "Usuario registrado")
