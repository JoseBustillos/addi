from controller.login import get_user_password
from interface.constant import window_geometry
from interface.register import openNewWindow

try:
    import Tkinter as tk
except:
    import tkinter as tk


window = tk.Tk()
window.title("ADDI")
window.geometry(window_geometry)

user = tk.Entry()
user.place(x=50, y=50)
user.pack()

password = tk.Entry(show="*")
password.place(x=50, y=50)
password.pack()

button_register = tk.Button(
    window,
    bg="blue",
    text="Ingresar",
    command=lambda: get_user_password(user, password),
)
button_register.pack()
button_newuser = tk.Button(
    window, bg="blue", text="Registrar", command=lambda: openNewWindow(window)
)
button_newuser.pack()
window.mainloop()
