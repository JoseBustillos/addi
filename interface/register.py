from controller.registerUser import registerUser
from interface.constant import window_geometry

try:
    import Tkinter as tk
except:
    import tkinter as tk


def openNewWindow(window):
    newWindow = tk.Toplevel(window)
    newWindow.title("Registro del paciente")
    newWindow.geometry(window_geometry)

    all_name = tk.Entry(newWindow)
    all_name.place(x=50, y=50)
    all_name.pack()

    user = tk.Entry(newWindow)
    user.place(x=50, y=50)
    user.pack()

    password = tk.Entry(newWindow, show="*")
    password.place(x=50, y=50)
    password.pack()

    birth_date = tk.Entry(newWindow)
    birth_date.place(x=50, y=50)
    birth_date.pack()

    button_newuser = tk.Button(
        newWindow,
        bg="blue",
        text="Registrar",
        command=lambda: registerUser(all_name, user, password, birth_date),
    )
    button_newuser.pack()
